import com.typesafe.scalalogging.LazyLogging

class CurrySample {

}

object CurrySample extends App with LazyLogging{

  def sum(n: Int)(m: Int) : Int = n + m
  val x = sum(5) _
  println(x) //Object
  val y = x(10)
  println(y) // 15

  def sum2(f: Int => Int): (Int, Int) => Int = {
      def sumF(a: Int, b: Int): Int = {
      if (a > b) 0
      else f(a) + sumF(a + 1, b)
    }
    sumF
  }

  def intToInt = sum2(x => x * x)

  println("@@@@@@@   " + intToInt(10, 25))


  /**
    * Uncurried
    */
  def add(x:Int)(y:Int) = x + y
  val addUncurried = Function.uncurried(add _)

  println(add(3)(4))  // 7
  println(addUncurried(3, 4))  // 7
//
//  logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", new Throwable("main.0427f58d561869703082.bundle.js:1 Cannot set property 'donations' of undefined TypeError: Cannot set property 'donations' of undefined\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.__tryOrUnsub (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t.notifyNext (main.0427f58d561869703082.bundle.js:1)"))
//  logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
//  logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", "main.0427f58d561869703082.bundle.js:1 Cannot set property 'donations' of undefined TypeError: Cannot set property 'donations' of undefined\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.__tryOrUnsub (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t.notifyNext (main.0427f58d561869703082.bundle.js:1)")

  val s = "main.0427f58d561869703082.bundle.js:1 Cannot set property 'donations' of undefined TypeError: Cannot set property 'donations' of undefined\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.__tryOrUnsub (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t._next (main.0427f58d561869703082.bundle.js:1)\n    at t.next (main.0427f58d561869703082.bundle.js:1)\n    at t.notifyNext (main.0427f58d561869703082.bundle.js:1)"
  def logggg(log: Log): Unit = {
//    logger.warn(log.message , log.trace.map(m => new Throwable(m)))
    logger.warn(log.message , log.trace match {
      case Some(m) => new Throwable(m)
      case None =>
    })
  }

  def trace(log: Log): Option[Throwable] = {
    log.trace match {
      case Some(st) => Some(new Throwable(st))
        case  None => None
    }
  }

//
//logggg(Log("Just message"))
//logggg(Log("Some message", Some("TTTTTTTTTTTTTTTTTTTTTT")))
//logggg(Log("Some message", Some(s)))

  val lo = Log("Some message LOLOLO", Some(s))
  val lol = Log("Some message LOLOLOllllll")
  logger.warn(lo.message, trace(lo).getOrElse(""))
  logger.warn(lol.message, trace(lol).getOrElse(""))


}

case class Log(message: String, trace: Option[String] = None)
