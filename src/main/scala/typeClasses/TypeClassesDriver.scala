package typeClasses

class TypeClassesDriver {

}

object TypeClassesDriver extends App {

  import BehavesLikeHumanInstances.dogBehavesLikeHuman

  val rover = Dog("Rover")

//  // (3a) apply the functions to the typeClasses.Dog instance
//  import typeClasses.BehavesLikeHuman.{speak, eatHumanFood}  //can also import the functions
//  typeClasses.BehavesLikeHuman.speak(rover)(dogBehavesLikeHuman)
//  typeClasses.BehavesLikeHuman.eatHumanFood(rover)(dogBehavesLikeHuman)

  // (3b) import the function, and you can call it on the typeClasses.Dog instance
  import BehavesLikeHumanSyntax.BehavesLikeHumanOps
  rover.speak
  rover.eatHumanFood

  // the function isn't implemented for a typeClasses.Cat or typeClasses.Bird, so these won't work
  //speak(typeClasses.Cat("Garfield"))
  //speak(typeClasses.Bird("Polly"))

}

sealed trait Animal
final case class Dog(name: String) extends Animal
final case class Cat(name: String) extends Animal
final case class Bird(name: String) extends Animal

