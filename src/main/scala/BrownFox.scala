class BrownFox {

}

object BrownFox extends App {
  val part_1 = Array[String]("   9: dog    ", "  2  :quick   ", "4:       fox", " 7:  the   ", "5:  jumps")
  val part_2 = Array[String]("3    :brown", " 1  :   the", "  6:over", "8   :lazy", "10:   this", "   11  :  morning")

  val c = List.concat(part_1, part_2)
    .map(_.filterNot((x: Char) => x.isWhitespace).split(':'))
    .map { case Array(key, value) => key.toInt -> value
    }.sortBy(_._1)
    .map(_._2)
    .mkString("", " ", "")

  println(s"$c")



  val a = List(2, 4, 5, 5, 3, 6)

  println(a.reduce((a, b) => if (a > b) a else b))
}
