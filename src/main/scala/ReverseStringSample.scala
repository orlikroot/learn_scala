class ReverseStringSample {
}

object ReverseStringSample extends App {

  def reverse(s: String) = ("" /: s)((a, x) => x + a)

  println(reverse("qwerty"))
}

