
class GenericSample {

}

object GenericSample extends App {

  private def transform[T <: Enumeration](values: Seq[String], enum: T): List[T#Value] = {
//    values.map(_.split(",").toSeq.map(f => enum.withName(f)))
    values.toList.map(f => enum.withName(f))
  }

  println("@@@@@@ " + Seq("One", "Two").toString())
  println("@@@@@@ " + Seq("One", "Two").mkString(","))

  println("#####    " + transform(List("one"), SomeType).head)
}

object SomeType extends Enumeration {

  type SomeType = Value

  val One = Value(1, "one")
  val Two = Value(2, "two")
  val Three = Value(3, "three")
}
