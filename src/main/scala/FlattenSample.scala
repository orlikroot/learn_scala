
class FlattenSample {

}

object FlattenSample extends App {

  val xs = List("a", "b", "c", "d")

  val xsa = xs.map(_ + "a")
  val xsb = xs.map(_ + "b")
  val xsc = xs.map(_ + "c")
  val xsd = xs.map(_ + "d")

  println(("f" /: xs)( _ + " " + _))
  println((xs :\ "f")( _ + " " + _))
  println(xs.foldRight("f")(_ + "_" + _))
  println(xs.foldLeft("f")(_ + "_" + _))


  def flattenLeft[T](xss: List[List[T]]) =
    (List[T]() /: xss)(_ ::: _)

  def reverseLest[T](xs: List[T]) = (List[T]() /: xs){(ys, y) => y :: ys}

  val xxs = List(xsa, xsb, xsc, xsd)

  println(flattenLeft(xxs))
  println(reverseLest(xs))
}




