class OptionSample {

}

object OptionSample extends App {


  def foo(x: String): Int = {
    val y = Option(x)
    y.getOrElse("3").toInt
  }

  println(foo(_))


}

case class SomeClass(s: Int) {
  override def toString: String = super.toString
}
