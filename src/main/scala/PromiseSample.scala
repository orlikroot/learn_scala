import scala.concurrent.{Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global

class PromiseSample {

}


object PromiseSample extends App {

  val f = Future { 1 }
  val p = Promise[Int]()

  p completeWith f

  p.future onComplete  {
    case x => println(x)
  }

  def ff(x: Int, y: Int): Future[Int] = Future {x/y}
  val pp = Promise[Int]()

  pp completeWith ff(10,0)
  pp.future onComplete {
    case x => println(x)
    case _: Exception => println(0)
  }




  Thread.sleep(2000)
}