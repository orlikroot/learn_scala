import java.sql.Timestamp
import java.time.{ZoneOffset, ZonedDateTime}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

class CollectionSample {

}

object CollectionSample extends App{
  /**
    * List immutable
    */
  var list = List(10, 20, 30, 40, 50)
  list(2) //30
  println(list.mkString(":::"))

  val list2 =  list.drop(1)
  println(list(2)) //30
  println(list2(2)) //40

  var list3 = list.map(x => x+2)
  println(list3) //List(12, 22, 32, 42, 52)
  list3.drop(1)
  println(list3) //List(12, 22, 32, 42, 52) IMMUTABLE!!!!

  /**
    * Array mutable
    */

  val numbers = Array(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)

  println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + numbers.find(_ == 3))

  println(numbers(2)) //3
  numbers.update(2, 20)
  println(numbers(2)) //20

  /**
    * Set immutable
    */

  val set = Set(1,2,3,4,5,6,1,2,3,4,5)

  println(set) //Set(5, 1, 6, 2, 3, 4)
  var set2 = set.+(12)
  println(set) //Set(5, 1, 6, 2, 3, 4)
  println(set2) //Set(5, 1, 6, 2, 12, 3, 4)
  set2.+(15)
  println(set2) //Set(5, 1, 6, 2, 12, 3, 4) IMMUTABLE!!!

  /**
    * Tuple (кортеж) - коллекция без указания типов объектов коллекции
    */

  var hotSpot = ("localhost", 3128)
  println(hotSpot) //(localhost,3128)
  println(hotSpot._1) //localhost

  var hotSpot2 = (11 -> 12, "Hz")
  println(hotSpot2._1._1) //11
  println(hotSpot2._1._2) //12
  println(hotSpot2._2) //Hz

  /**
    * Map
    */
  var map = Map(1 -> "Hz", 2 -> "kHz")
  println(map(1)) //Hz
  map.+=(3 -> "dfgfd")
  println(map(3))

  /**
    * MutableList
    */

  println(list.map(x => x - 5)) //List(5, 15, 25, 35, 45)

  var z = 0
  list.foreach(x => z += x)
  println("Z = " + z) //150
  println(list) //List(10, 20, 30, 40, 50)
  var mList: scala.collection.mutable.MutableList[Int] = mutable.MutableList(1,2,4,3,6,5,9,8,7)
  // println(mList)
  // mList = mList.map(x => x + 10)
  // list = List(3,2,1)
  // println(mList)
  // println(list)


  mList.filter((i: Int) => i % 2 == 0)
  println(mList) //MutableList(1, 2, 4, 3, 6, 5, 9, 8, 7)
  println(mList.filter((i: Int) => i % 2 == 0)) //MutableList(2, 4, 6, 8)
  mList(1) = 33
  println(mList) //MutableList(1, 33, 4, 3, 6, 5, 9, 8, 7)

  var list4 = list.zip(list2)
  println(list4) //List((10,20), (20,30), (30,40), (40,50))


  /**
    * ListBuffer
    */

  var fruit = new ListBuffer[String]
  fruit += "apple"
  fruit += "banana"
  println(fruit)

  /**
    * foreach
    */
  var list6 = List(10, 20, 30, 40, 50, 60)
  var mutList: mutable.MutableList[Int] = new mutable.MutableList[Int]
  for(i <- list6 if i != 10 if i % 3 == 0){
    mutList.+=(i)
  }
  println(mutList) //MutableList(30, 60)

  //------------------------------

  var list7 = List(1,2,3,4,5,6,7)
  println("@@@@@@@@@@@@@@@@@@@@@@@@@@@ " + list7.filterNot(_ == 2))
  println( for (x <- list7 if x%2 == 0) yield x*10) //List(20, 40, 60)
  println(list7.filter(_%2 == 0).map(_*10)	) //List(20, 40, 60)
  println(for (x <- list6; y <- list7) yield x+y) //List(11, 12, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 47, 51, 52, 53, 54, 55, 56, 57, 61, 62, 63, 64, 65, 66, 67)
  println(list6 flatMap {x => list7 map {y => x+y}}) //List(11, 12, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 47, 51, 52, 53, 54, 55, 56, 57, 61, 62, 63, 64, 65, 66, 67)

  for (x <- list6; y <- list7) {
    print("%d/%d = %.1f  ".format(x, y, x/y.toFloat)) // 10/1 = 10.0  10/2 = 5.0  10/3 = 3.3  10/4 = 2.5  10/5 = 2.0  10/6 = 1.7  10/7 = 1.4  20/1 = 20.0  20/2 = 10.0  20/3 = 6.7  20/4 = 5.0  20/5 = 4.0  20/6 = 3.3  20/7 = 2.9  30/1 = 30.0  30/2 = 15.0  30/3 = 10.0  30/4 = 7.5  30/5 = 6.0  30/6 = 5.0  30/7 = 4.3  40/1 = 40.0  40/2 = 20.0  40/3 = 13.3  40/4 = 10.0  40/5 = 8.0  40/6 = 6.7  40/7 = 5.7  50/1 = 50.0  50/2 = 25.0  50/3 = 16.7  50/4 = 12.5  50/5 = 10.0  50/6 = 8.3  50/7 = 7.1  60/1 = 60.0  60/2 = 30.0  60/3 = 20.0  60/4 = 15.0  60/5 = 12.0  60/6 = 10.0  60/7 = 8.6
  }
  println()

  //------------------------------

  var mutList2: mutable.MutableList[Int] = new mutable.MutableList[Int]
  list6.foreach(a => mutList2.+=(a + 13))
  println(mutList2) //MutableList(23, 33, 43, 53, 63, 73)

  /**
    * Partitioning
    */

  var listPart = list6.partition(x => x % 3 == 0)
  println(listPart) //(List(30, 60),List(10, 20, 40, 50))

  /**
    * groupBy
    */

  var listGroup = list6.groupBy( _ != 30)
  println(listGroup) //Map(false -> List(30), true -> List(10, 20, 40, 50, 60))

  /**
    * span
    */
  var listSpan = list6.span(u => u < 39)
  println(listSpan) //(List(10, 20, 30),List(40, 50, 60))

  /**
    * splitAt
    */
  var listSplit = list6.splitAt(4)
  println(listSplit) //(List(10, 20, 30, 40),List(50, 60))

  /**
    * sliding
    */

    var nums = (1 to 11).toArray
  var numsSliding = nums.sliding(2) //List(1, 2) List(2, 3) List(3, 4) List(4, 5) List(5, 6) List(6, 7) List(7, 8) List(8, 9) List(9, 10)
  var numsSliding2 = nums.sliding(3, 3) //List(1, 2, 3) List(4, 5, 6) List(7, 8, 9) List(10)
  var numsSliding3 = nums.sliding(2, 3) //List(1, 2) List(4, 5) List(7, 8) List(10)
  var numsSliding4 = nums.sliding(2,2) //List(1, 2) List(4, 5) List(7, 8) List(10)
//  numsSliding.toList.foreach(e => print(" " + e.toList))
//  numsSliding2.toList.foreach(e => print(" " + e.toList))
//  numsSliding3.toList.foreach(e => print(" " + e.toList))
  numsSliding4.toList.foreach(e => print(" " + e.toList))
  println(nums.slice(0, 5).toList.foreach(e => print("WWWWWWWWW " + e)))
  println(nums.slice(2, 8).toList.foreach(e => print(" " + e)))


  val date = ZonedDateTime.now(ZoneOffset.UTC)
  println(date)
  println(Timestamp.from(date.toInstant))

  val listNotUnique = List(10, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5).distinct

  println(listNotUnique)

  def r = Random

  println("qazxswedcvfrtgbnhyujmkiolpQWEDSAZXCVFRTGBNHYMJUIKLOP-_"
    .toSet.toList.sortWith(_.toInt > r.nextInt() + _).mkString)


  println(10000001000L % 54)

  val s = Seq(1, 2, 3)
  println(s.lengthCompare(0))
}

