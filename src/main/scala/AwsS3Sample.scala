import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.{GetObjectRequest, ListVersionsRequest}

import scala.collection.JavaConverters._
import scala.io.Source

class AwsS3Sample {

}

object AwsS3Sample extends App {

  val region = Regions.US_EAST_1
  val bucketName = "sun-b2c-health-prediction-nonprod"
  val key = "logs/nightingale-predict/model=Pain/ingestDate=2019-09-17/ingestHour=00/predictDate=20190916/zip=010/20190917T000621.json"

  val s3Client = AmazonS3ClientBuilder.standard()
    .withCredentials(new ProfileCredentialsProvider("sun-dev_us-east-1"))
    .withRegion(region)
    .build()


  val request = new ListVersionsRequest()
    .withBucketName(bucketName)
    .withPrefix("logs/nightingale-predict/model=Pain/ingestDate=2019-09-17/ingestHour=00/predictDate=20190916/zip=010/20190917T000621.json1")
    .withMaxResults(20)
  val versionListing = s3Client.listVersions(request)


  val object1 = s3Client.getObject(bucketName, key)

  val object2 =s3Client.getObject(new GetObjectRequest(bucketName, key))

  println(Option(object2.getObjectMetadata.getVersionId))

  val content = Source.fromInputStream(object1.getObjectContent).getLines.mkString("\n")

  println(s"${content}")

  versionListing.getVersionSummaries.asScala.map(os => println(s"Retrieved object ${os.getKey}, version ${os.getVersionId}, ${os.isLatest}"))
}
