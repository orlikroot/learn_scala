import java.time.ZonedDateTime

class PatternMatchingSample {

}

object PatternMatchingSample extends App {

  class Cat extends Enumeration

  abstract class Filter
    case class Date(value: ZonedDateTime) extends Filter
    case class Category(value: Cat) extends Filter
    case class InStock(value: Boolean) extends Filter
    case class Price(from: BigDecimal, to: BigDecimal) extends Filter


  def applyFilter(filter: Filter): String = filter match {
    case Date(_) => "Filter by date"
    case Category(_) => "Filter by date"
  }

  println(applyFilter(Date(ZonedDateTime.now)))
}


