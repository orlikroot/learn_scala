import java.net.URL
import java.util.UUID
//import scala.reflect.runtime.universe._

class JavaSample {

  class JavaP(acc_ : String){
    val acc = acc_
  }

  def y = new JavaP("rty").acc

  object JavaO{
    val z: Int = 123

    def get = z
  }


//  import scala.reflect.
//
//  def matchList[A: TypeTag](list: List[A]) = list match {
//    case strlist: List[String @unchecked] if typeOf[A] =:= typeOf[String] => println("A list of strings!")
//    case intlist: List[Int @unchecked] if typeOf[A] =:= typeOf[Int] => println("A list of ints!")
//  }

  import scala.reflect.{ClassTag, classTag}

  def matchList2[A : ClassTag](list: List[A]) = list match {
    case strlist: List[String @unchecked] if classTag[A] == classTag[String] => println("A List of strings!")
    case intlist: List[Int @unchecked] if classTag[A] == classTag[Int] => println("A list of ints!")
  }

}

object JavaSample extends App{
  def r = new JavaSample()
  println(r.JavaO.get)
  println(r.y)

  r.matchList2(List(1,2,3))
  r.matchList2(List("1","2","3"))

def get(d: Int): String = {d.toString}
  get(5)


  val aURL = new URL("http://example.com:80/docs/books/tutorial/index.html?name=networking#DOWNLOADING")

  println(aURL.toString)
  println(aURL.getHost)


}
