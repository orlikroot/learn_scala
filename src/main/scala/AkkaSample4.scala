import akka.actor.{Actor, ActorSystem, Props}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class AkkaSample4 {

}

class SendMessageActor extends Actor {
  override def receive: Receive = {
    case "message" => {
      println("Send message " + this.context.self.toString())
    }
    case "message2" => {
      println("Send message2 " + this.context.self.toString())
    }
  }
}


object AkkaSample4 extends App{



  val system = ActorSystem("actorSystem")
  val sendMessageActor = system.actorOf(Props[SendMessageActor], "sendMessageActor")

  val cancellable = system.scheduler.schedule(0 milliseconds, 50 millisecond, sendMessageActor, "message")
  val cancellable2 = system.scheduler.schedule(0 milliseconds, 150 millisecond, sendMessageActor, "message2")


}
