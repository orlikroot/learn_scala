

import scala.concurrent.{Await, Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success}

import org.scalatest.concurrent.ScalaFutures._

class FutureSample {

}

object FutureSample extends App {

//  implicit def futureToValue[V](future: Future[V]): V = Await.result(future, 3 seconds)

  val fut = Future { Thread.sleep(1000); 21 + 21}

  println(fut.isCompleted)
  println(fut.value)

  val doubleFut = fut.map(x => x * 2)

  val successFut = Future.successful(33)

  val failedFut = Future.failed(new Throwable("Bad future"))

  val successFromTry = Future.fromTry(Success( { 21 + 21 }))

  val failedFromTry = Future.fromTry(Failure( { new Throwable("Bad future") }))

  println(doubleFut)
  println(doubleFut.isCompleted)

  val result = for {
    f1 <- fut
    f2 <- doubleFut
    f3 <- successFut
  } yield f1 + f2 + f3

  println(failedFut.value)
  //Wait complete of future
//  println(Await.result(result, 3 seconds))


  //Future with promise
  val pro = Promise[Int]

  val fut2 = pro.future

  println("Non completed future " + fut2.value)
  pro.success(55)
  println("Completed future " + fut2.value)
  //------------------------


  def divide(x: Int, y: Int): Future[Int] = {
    Future{
      {x / y}
    }
  }

  //recover
  def safeDivide(x: Int, y: Int): Future[Int] = divide(x,y) recover {
    case _: ArithmeticException => 0
  }
  println(Await.result(safeDivide(10, 2), 3 seconds))
  println(Await.result(safeDivide(10, 0), 3 seconds))

  //recoverWith
  def safeDivide2(x: Int, y: Int): Future[Int] = divide(x, y) recoverWith {
    case _: ArithmeticException => Future { y }
  }

//  println(Await.result(safeDivide2(10, 2), 3 seconds))
//  println(Await.result(safeDivide2(10, 0), 3 seconds))

  //transform
  def trans(x: Int, y: Int) = divide(x, y) transform (
    res => (res * 11).toString,
    ex => new ArrayIndexOutOfBoundsException(ex.getMessage)
  )

  def transSuccess(x: Int, y: Int) = divide(x, y) transform {
    case Success(res) => Success(res * 11)
    case Failure(_) => Success(0)
  }

//  println(Await.result(trans(10, 0), 3 seconds))
//  println(Await.result(trans(10, 5), 3 seconds))

//    println(Await.result(transSuccess(10, 0), 3 seconds))
//    println(Await.result(transSuccess(100, 5), 3 seconds))

  val listFut = List(fut, doubleFut, successFut)

  val tupleFut = fut zip doubleFut
  println(Await.result(tupleFut, 3 seconds))

  val oneFut = Future(divide(10,5)).flatten

  def transWith(x: Int, y: Int) = divide(x, y) transformWith  {  //Scala 2.12
    case Success(value) => Future {throw new NoSuchElementException(value.toString)}
    case Failure(exception) => Future { x + y }
  }

//  println(Await.result(transWith(10, 0), 3 seconds))
//  println(Await.result(transWith(10, 5), 3 seconds))


  println(divide(10,5).futureValue)  //Use for test import org.scalatest.concurrent.ScalaFutures._
  println(divide(10,0).futureValue)


  fut.flatMap {a =>
    doubleFut.flatMap {b =>
      successFut.flatMap {c =>
        println("LAST FUTURE: " + c)
        Future.successful()
      }

    }

  }

}
