import scala.io.StdIn
import scala.util.Try

class HadoopExample {

}

object HadoopExample extends App {

//  4.1 Алгоритмы на MapReduce
//    4 out of 15 steps passed
//    9 out of 46 points  received
//    Напишите программу, которая реализует In-mapper combining v.1 для задачи WordCount в Hadoop Streaming.
//  Sample Input:
//
//    aut Caesar aut nihil
//      aut aut
//      de mortuis aut bene aut nihil
//      Sample Output:
//
//  nihil	1
//  aut	2
//  Caesar	1
//  aut	2
//  nihil	1
//  aut	2
//  de	1
//  bene	1
//  mortuis	1


//  var input = scala.io.StdIn.readLine()
//  do {
//    input.trim.split(" ").toList.groupBy(identity).mapValues(_.size).foreach(c => println(c._1 + "\t" + c._2))
//    input = scala.io.StdIn.readLine()
//  } while (input != null)


//  4.1 Алгоритмы на MapReduce
//    4 out of 15 steps passed
//    9 out of 46 points  received
//    Напишите программу, которая реализует In-mapper combining v.2 для задачи WordCount в Hadoop Streaming.
//  Sample Input:
//
//    aut Caesar aut nihil
//      aut aut
//      de mortuis aut bene aut nihil
//      Sample Output:
//
//  aut	6
//  mortuis	1
//  bene	1
//  Caesar	1
//  de	1
//  nihil	2

//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  while ({line = StdIn.readLine(); line != null}) {
//    string.append(" " + line)
//  }
//  string.toString().trim.split(" ").toList.groupBy(identity).mapValues(_.size).foreach(c => println(c._1 + "\t" + c._2))

//  4.1 Алгоритмы на MapReduce
//    6 out of 15 steps passed
//    19 out of 46 points  received
//    Реализуйте reducer в задаче подсчета среднего времени, проведенного пользователем на странице.
//
//  Mapper передает в reducer данные в виде key / value, где key - адрес страницы, value - число секунд, проведенных пользователем на данной странице.
//
//    Среднее время на выходе приведите к целому числу.
//
//  Sample Input:
//
//    www.facebook.com	100
//  www.google.com	10
//  www.google.com	5
//  www.google.com	15
//  www.stepic.org	60
//  www.stepic.org	100
//  Sample Output:
//
//    www.facebook.com	100
//  www.google.com	10
//  www.stepic.org	80


////  var line = ""
//  var string: StringBuilder = new StringBuilder
////  while ({line = scala.io.StdIn.readLine(); line != null}) {
////    string.append("," + line)
////  }
//
//  implicit class Pairs[A, B](p: List[(A, B)]) {
//
//    def getCount(s: A) = p.count(_._1 == s)
//
//    def toMultiMap: Map[A, List[B]] = p.groupBy(_._1).mapValues(_.map(_._2))
//  }
//
//  implicit class ImplDoubleVecUtils(values: Seq[Int]) {
//    def mean = (values.sum / values.length) + ";" + values.length
//  }
//
//  string.append("www.facebook.com\t100\nwww.google.com\t10\nwww.google.com\t5\nwww.google.com\t15\nwww.stepic.org\t60\nwww.stepic.org\t100")
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val a = s.trim.split("\t")
//    val b = Tuple2(a(0), a(1))
//    println(b)
//    b
//  })
//    val range = n.map(_._1).zipWithIndex.toMap
//    val n2 = n.toMultiMap.map(x => {
//        (x._1, x._2.map(_.toInt).mean)
//      }
//    )
//
//  def ordering(s1: String, i1: String)(s2: String, i2: String): Boolean = {
//    range(s1) > range(s2)
//  }
//
//  val x1 = collection.mutable.LinkedHashMap(n2.toSeq.sortWith((a, c) => ordering(a._1, a._2) (c._1, c._2)):_*)
//  x1.foreach(x => println(x._1 + "\t" + x._2))


//Mapper принимает на вход строку, содержащую значение и через табуляцию список групп, разделенных запятой.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
////  while ({line = scala.io.StdIn.readLine(); line != null}) {
////    string.append("\n" + line)
////  }
//    string.append("1\ta,b\n2\ta,d,e\n1\tb\n3\ta,b")
//
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//        val a = s.trim.split("\t")
//        val b = Tuple2(a(0), a(1).trim.split(",").toVector)
//        b
//      }).flatMap(s => s._2.map(a => s._1 -> a)).map(p => println(s"${p._1},${p._2}\t1"))


//  //Reducer принимает на вход данные, созданные mapper из предыдущей шага.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1,a\t1\n1,b\t1\n1,b\t1\n2,a\t1\n2,d\t1\n2,e\t1\n3,a\t1\n3,b\t1")
//  import ListUtil._
//    val n = string.toString().trim.split("\n").toList.map(s => {
//          val a = s.trim.split("\t")
//          val b = a(0).trim.split(",")
//          val c = Tuple2(b(0), b(1))
//          c
//        })
//    val nc = dedupe(n)
//
//  nc.foreach(c => println(s"${c._1},${c._2}"))


//  //Reducer принимает на вход данные, созданные mapper из предыдущей шага.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1,a\n2,a\n3,a\n1,b\n3,b\n2,d\n2,e")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val a = s.trim.split(",")
//    val b = Tuple2(a(0), a(1))
//    b
//  })
//
//  n.foreach(c => println(s"${c._2}\t1"))

//  //Reducer принимает на вход данные, созданные mapper из предыдущей шага.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1\ta\n1\tb\n1\tb\n2\ta\n2\td\n2\te\n3\ta\n3\tb")
//  import ListUtil._
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val a = s.trim.split("\t")
//    Tuple2(a(0),a(1))
//  })
//
//  val r = dedupe(n).map(_._2)
//
//  val c = r.groupBy(identity).mapValues(_.size).toVector.sortBy(it => it._1)
//
//  c.foreach(cc => println(s"${cc._1}\t${cc._2}"))

//  //Reducer принимает на вход данные, созданные mapper из предыдущей шага.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("a b\na b a c")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    s.trim.split(" ")
//  })
//
//    val y = n.flatMap(as => {
//      as.map(s => {
//        as.filterNot(f => f == s).map { c =>
//          Tuple2(s, c)
//        }
//      })
//    }).flatten
//
//  y.map(c => println(s"${c._1},${c._2}\t1"))

//  //Reducer принимает на вход данные, созданные mapper из предыдущей шага.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("a b\na b a c")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    s.trim.split(" ")
//  })
//
//  val y = n.flatMap(as => {
//    as.map(s => {
//      s -> as.filterNot(_.equals(s))
//    })
//  }).map(r => r._1 -> r._2.groupBy(identity).mapValues(_.size))
//
//  y.map(c => println(s"${c._1}\t${c._2.map(d => s"${d._1}:${d._2}").mkString(",")}"))


//  //4.2.1
//  //Дан файл с логами переходов пользователей. Каждая строка состоит из 3 полей: время перехода (unix timestamp), ID пользователя, URL, на который перешел пользователь.
//  //
//  //Напишите mapper с помощью Hadoop Streaming, печатающий только те строки из файла, которые соответствуют пользователю user10.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1448713968\tuser2\thttps://ru.wikipedia.org/\n1448764519\tuser10\thttps://stepic.org/\n1448713968\tuser5\thttp://google.com/\n1448773411\tuser10\thttps://stepic.org/explore/courses\n1448709864\tuser3\thttp://vk.com/")
//
//  val predicate = "user10"
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val c = s.trim.split("\t")
//    val r = Tuple3(c(0), c(1), c(2))
//    if (r._2 == predicate) println(s)
//  })

//  //4.2.2
//  //Дан файл с логами переходов пользователей. Каждая строка состоит из 3 полей: время перехода (unix timestamp), ID пользователя, URL, на который перешел пользователь.
  //
  //Напишите mapper с помощью Hadoop Streaming, печатающий URL из каждой строки.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1448713968\tuser2\thttps://ru.wikipedia.org/\n1448764519\tuser10\thttps://stepic.org/\n1448713968\tuser5\thttp://google.com/\n1448773411\tuser10\thttps://stepic.org/explore/courses\n1448709864\tuser3\thttp://vk.com/")
//
//  val predicate = "user10"
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val c = s.trim.split("\t")
//    val r = Tuple3(c(0), c(1), c(2))
//    println(r._3)
//  })

//  //4.2.3
//  //Напишите reducer, который объединяет элементы из множества A и B. На вход в reducer приходят пары key / value, где key - элемент множества, value - маркер множества (A или B)
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1\tA\n2\tA\n2\tB\n3\tB")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val c = s.trim.split("\t")
//    val r = Tuple2(c(0), c(1))
//    r._1
//  }).distinct.map(s => println(s))

//  //4.2.4
//  //Напишите reducer, который делает пересечение элементов из множества A и B. На вход в reducer приходят пары key / value,
//  // где key - элемент множества, value - маркер множества (A или B)
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1\tA\n2\tA\n2\tB\n3\tB")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val c = s.trim.split("\t")
//    val r = Tuple2(c(0), c(1))
//    r
//  }).groupBy(_._1).mapValues(_.map(_._2)).filter(p => p._2.size > 1).keys
//  n.map(println)

//  //4.2.7
//  //Напишите reducer, реализующий объединение двух файлов (Join) по id пользователя. Первый файл содержит 2 поля через табуляцию: id пользователя и запрос в поисковой системе. Второй файл содержит id пользователя и URL, на который перешел пользователь в поисковой системе.
//  //
//  //Mapper передает данные в reducer в виде key / value, где key - id пользователя, value состоит из 2 частей: маркер файла-источника (query или url) и запрос или URL.
//  //
//  //Каждая строка на выходе reducer должна содержать 3 поля, разделенных табуляцией: id пользователя, запрос, URL.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("user1\tquery:гугл\nuser1\turl:google.ru\nuser2\tquery:стэпик\nuser2\tquery:стэпик курсы\nuser2\turl:stepic.org\nuser2\turl:stepic.org/explore/courses\nuser3\tquery:вконтакте")
//
//  val queryStr = "query"
//  val urlStr = "url"
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val c = s.trim.split("\t")
//    val d = c(1).trim.split(":")
//    val g = Tuple2(d(0), d(1))
//    val r = Tuple2(c(0), g)
//    r
//  }).groupBy(_._1).mapValues(_.map(_._2)).map(h => {h._1 -> (h._2.groupBy(_._1).mapValues(_.map(_._2)))})
//
//  n.map(user => {
//    if (user._2.contains(queryStr) && user._2.contains(urlStr)) {
//      val querys = user._2(queryStr)
//      val urls = user._2(urlStr)
//      querys.map(q => {
//        urls.map(u => {
//          println(s"${user._1}\t$q\t$u")
//        })
//      })
//    }
//  })

//  //4.3.1
////  Реализуйте mapper первой mapreduce задачи для расчета TF-IDF с помощью Hadoop Streaming.
////
////  Формат входных данных следующий: каждая строка содержит номер документа и строку из него, разделенные ":". Ключ выходных данных является составным: он содержит слово документа и его номер, разделенные "#".
////
////  Слово в документе - последовательность символов (букв или цифр), не содержащая пробельных символов и знаков пунктуации.
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
////  string.append("1:aut Caesar aut nihil\n1:aut aut\n2:de mortuis aut bene aut nihil")
//  string.append("1:xxx:xxx:xxx\n2:xxx::xxx::xxx::\n3:xxx:::xxx:::xxx:::")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    val f = s.trim.split("[^\\w+]")
//    Tuple2(f(0), f.toList.tail.filterNot(p => p == ""))
//  }).groupBy(_._1).mapValues(_.map(_._2))
//  import scala.collection.immutable.ListMap
//  val b = ListMap(n.toSeq.sortBy(_._1):_*)
//
//  b.map(v => v._2.flatten.map(s => println(s"${s}#${v._1}\t1")))

//  //4.3.2
////  Реализуйте reducer первой mapreduce задачи для расчета TF-IDF с помощью Hadoop Streaming.
////
////Ключ входных данных составной: он содержит слово и номер документа через "#".
////
////Ключом в выходных данных является слово, а значение состоит из двух элементов, разделенных табуляцией: номер документа и tf (сколько раз данное слово встретилось в данном документе).
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("aut#1\t1\naut#1\t1\naut#1\t1\naut#1\t1\naut#2\t1\naut#2\t1\nbene#2\t1\nde#2\t1\nmortuis#2\t1\nnihil#1\t1\nnihil#2\t1\nCaesar#1\t1")
//
//  val n = string.toString().trim.split("\n").toList.map(s => {
//    s.trim.split("\t")(0)
//  }).map(x => {
//    val a = x.trim.split("#")
//    Tuple2(a(0), a(1))
//  }).toVector
//
//  val a = n.foldLeft(List.empty[((String, String), Int)])((x, y) => {
//    if (x.map(_._1).contains(y)) {
//      val count = x.filter(f => f._1 == y).head._2
//      val acc = x.filterNot(f => f._1 == y)
//      acc ++ List((y, count + 1))
//    } else {
//      x ++ List((y, 1))
//    }
//  })
//
//  a.map(x => println(s"${x._1._1}\t${x._1._2}\t${x._2}"))

//  //4.3.3
//  //  Реализуйте mapper ﻿второй mapreduce задачи для расчета TF-IDF с помощью Hadoop Streaming.
//  //
//  //Во входных данных ключом является слово, а значение состоит из номера документа и tf, разделенных табуляцией.
//  //
//  //Значение в выходных данных - это тройка: номер документа, tf и единица, разделенные ";".
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("aut\t1\t4\naut\t2\t2\nbene\t2\t1\nde\t2\t1\nmortuis\t2\t1\nnihil\t1\t1\nnihil\t2\t1\nCaesar\t1\t1")
//
//  string.toString().trim.split("\n").toList.map(s => {
//    val a = s.trim.split("\t")
//    println(s"${a(0)}\t${a(1)};${a(2)};1")
//  })

  //  //4.3.4
  //  //  Реализуйте reducer второй mapreduce задачи для расчета TF-IDF с помощью Hadoop Streaming.
  //  //
  //  //Входные данные: ключ - слово, значение - тройка: номер документа, tf слова в документе и 1 (разделены ';').
  //  //
  //  //Выходные данные: ключ - пара: слово, номер документа (разделены '#'), значение - пара: tf слова в документе, n - количество документов с данным словом (разделены табуляцией).
  //  var line = ""
  //  var string: StringBuilder = new StringBuilder
  //  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
  //  //    string.append("\n" + line)
  //  //  }
  //  string.append("aut\t1;4;1\naut\t2;2;1\nbene\t2;1;1\nde\t2;1;1\nmortuis\t2;1;1\nnihil\t1;1;1\nnihil\t2;1;1\nCaesar\t1;1;1")
  //
  //  val r = string.toString().trim.split("\n").toList.map(s => {
  //    val a = s.trim.split("\t")
  //    val b = a(1).trim.split(";")
  //    Tuple4(a(0), b(0).toInt, b(1).toInt, b(2).toInt)
  //  })
  //
  //
  //
  //  val res = r.map(x => {
  //    x.copy(_4 = r.map(_._1).count(s => s == x._1))
  //  })
  //
  //  res.map(s => println(s"${s._1}#${s._2}\t${s._3}\t${s._4}"))

  //  //5.2.1
  //  //  Реализуйте алгоритм Дейкстры поиска кратчайшего пути в графе.
  //  //
  //  //Входные данные: В первой строке указаны два числа: число вершин и число ребер графа. Далее идут строки с описанием ребер. Их количество равно числу ребер. В каждой строке указаны 3 числа: исходящая вершина, входящая вершина, вес ребра. В последней строке указаны 2 номера вершины: начальная и конечная вершина, кратчайший путь между которыми нужно найти.
  //  //
  //  //Выходные данные: минимальное расстояние между заданными вершинами. Если пути нет, то нужно вернуть -1.
  //  var line = ""
  //  var string: StringBuilder = new StringBuilder
  //  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
  //  //    string.append("\n" + line)
  //  //  }
  //  string.append("4 8\n1 2 6\n1 3 2\n1 4 10\n2 4 4\n3 1 5\n3 2 3\n3 4 8\n4 2 1\n1 4")
  //
  //  val r = string.toString().trim.split("\n").toVector
  //
  //  val data1 = r.head.trim.split(" ")
  //  val tipCount: Int = data1(0).toInt
  //  val edgeCount = data1(1)
  //
  //  val data = (r.drop(1) dropRight 1).map(s => {
  //    val a = s.trim.split("\n")
  //    Edge(a(0), a(1), a(2).toInt)
  //  })
  //
  //  val data3 = r.last.trim.split(" ")
  //  val startTip = data3(0)
  //  val endTip = data3(1)
  //
  //  val allTips = (1 to tipCount toVector).map(i => Tip(i.toString, None))
  //
  //
  //  def calc(ratedList: List[Tip], allTips: List[Tip], startTip: Tip, entTip: Tip) = {
  //    val unratedList = allTips.filterNot(t => ratedList.map(_.name).contains(t.name))
  //
  //
  //
  //
  //
  //  }
  //
  //
  //  allTips.foldLeft(List[Tip])((x,y) => {
  //
  //  })

//  //  5.2.2 Реализуйте mapper в задаче поиска кратчайшего пути с помощью Hadoop Streaming.
//  //
//  //    Входные и выходные данные: в качестве ключа идет номер вершины, значение состоит из двух полей, разделенных табуляцией:
//  //
//  //    Минимальное расстояние до данной вершины (если его еще нет, то пишется INF)
//  //  Список исходящих вершин (через "," в фигурных скобках)
//  var line = ""
//  var string: StringBuilder = new StringBuilder
//  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
//  //    string.append("\n" + line)
//  //  }
//  string.append("1\t0\t{2,3,4}\n2\t1\t{5,6}\n3\t1\t{}\n4\t1\t{7,8}\n5\tINF\t{9,10}\n6\tINF\t{}\n7\tINF\t{}\n8\tINF\t{}\n9\tINF\t{}\n10\tINF\t{}")
//  //  string.append("1\t0\t{2}")
//
//  val r = string.toString().trim.split("\n").toList.map(s => {
//    val a = s.trim.split("\t")
//    val b = a(2).drop(1).dropRight(1).trim.split(",").toVector.filterNot(s => s == "")
//
//    val c = Try{
//      Option(a(1).toInt)
//    }.getOrElse(None)
//    Point(a(0), c, b)
//  })
//
//  r.map(point => {
//    point.printPoint
//    point.neighbors.map(ns => {
//      Point(ns, point.distance.map(i => i + 1), Vector.empty).printPoint
//    })
//  })

  //    //5.3.1
  //    //  Реализуйте mapper для алгоритма расчета PageRank с помощью Hadoop Streaming.
  //    //
  //    //Входные и выходные данные: В качестве ключа идет номер вершины. Значение составное:
  //    // через табуляцию записано значение PageRank (округленное до 3-го знака после запятой) и
  //    // список смежных вершин (через "," в фигурных скобках).
  //    var line = ""
  //    var string: StringBuilder = new StringBuilder
  //    //  while ({line = scala.io.StdIn.readLine(); line != null}) {
  //    //    string.append("\n" + line)
  //    //  }
  //    string.append("1\t0.200\t{2,4}\n2\t0.200\t{3,5}\n3\t0.200\t{4}\n4\t0.200\t{5}\n5\t0.200\t{1,2,3}")
  //
  //    val r = string.toString().trim.split("\n").toList.map(s => {
  //      val a = s.trim.split("\t")
  //      val b = a(2).dropRight(1).drop(1).trim.split(",").toList
  //      Point531(a(0),  BigDecimal(a(1).toDouble).setScale(3, BigDecimal.RoundingMode.HALF_UP).toDouble, b)
  //    })
  //
  //  r.map(p => {
  //    p.pr
  //    val prD =  BigDecimal(p.pageRank / p.adjacentVertices.length).setScale(3, BigDecimal.RoundingMode.HALF_UP).toDouble
  //    p.adjacentVertices.map(n => {
  //      Point531(n, prD, List.empty).pr
  //    })
  //  })

  //  //5.3.2
  //  //  Реализуйте reducer для алгоритма расчета PageRank с помощью Hadoop Streaming. Используйте упрощенный алгоритм (без случайных переходов).
  //  //
  //  //Входные и выходные данные: В качестве ключа идет номер вершины. Значение составное: через табуляцию записано
  //  // значение PageRank (округленное до 3-го знака после запятой) и список смежных вершин (через "," в фигурных скобках).
  //  var line = ""
  //  var string: StringBuilder = new StringBuilder
  //  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
  //  //    string.append("\n" + line)
  //  //  }
  //  string.append("1\t0.067\t{}\n1\t0.200\t{2,4}\n2\t0.067\t{}\n2\t0.100\t{}\n2\t0.200\t{3,5}\n3\t0.067\t{}\n3\t0.100\t{}\n3\t0.200\t{4}\n4\t0.100\t{}\n4\t0.200\t{}\n4\t0.200\t{5}\n5\t0.100\t{}\n5\t0.200\t{}\n5\t0.200\t{1,2,3}")
  //
  //  val r = string.toString().trim.split("\n").toList.map(s => {
  //    val a = s.trim.split("\t")
  //    val b = a(2).dropRight(1).drop(1).trim.split(",").toList.filterNot(p => p.isEmpty || p =="")
  //    Point531(a(0),  BigDecimal(a(1).toDouble).setScale(3, BigDecimal.RoundingMode.HALF_UP).toDouble, b)
  //  })
  //
  // val points = r.filter(_.adjacentVertices.nonEmpty)
  // val ranks = r.filter(_.adjacentVertices.isEmpty)
  //
  //  points.map(p => {
  //    p.copy(pageRank =BigDecimal(ranks.filter(p1 => p1.name == p.name).map(_.pageRank).sum).setScale(3, BigDecimal.RoundingMode.HALF_UP).toDouble
  //    ).pr
  //  })

  //5.3.3
  // Модифицируйте reducer из предыдущего задания так, чтобы он расcчитывал PageRank с учетом случайного перехода, т.е. первого члена в формуле:
  //
  //
  //
  //Для всех тестов считайте, что N = 5,  α = 0,1.
  //Входные и выходные данные: В качестве ключа идет номер вершины. Значение составное: через табуляцию записано значение
  // PageRank (округленное до 3-го знака после запятой) и список смежных вершин (через "," в фигурных скобках).
  var line = ""
  var string: StringBuilder = new StringBuilder
  //  while ({line = scala.io.StdIn.readLine(); line != null}) {
  //    string.append("\n" + line)
  //  }
  string.append("1\t0.067\t{}\n1\t0.200\t{2,4}\n2\t0.067\t{}\n2\t0.100\t{}\n2\t0.200\t{3,5}\n3\t0.067\t{}\n3\t0.100\t{}\n3\t0.200\t{4}\n4\t0.100\t{}\n4\t0.200\t{}\n4\t0.200\t{5}\n5\t0.100\t{}\n5\t0.200\t{}\n5\t0.200\t{1,2,3}")

  val r = string.toString().trim.split("\n").toList.map(s => {
    val a = s.trim.split("\t")
    val b = a(2).dropRight(1).drop(1).trim.split(",").toList.filterNot(p => p.isEmpty || p =="")
    Point531(a(0),  BigDecimal(a(1).toDouble).setScale(3, BigDecimal.RoundingMode.HALF_UP).toDouble, b)
  })

  val points = r.filter(_.adjacentVertices.nonEmpty)
  val ranks = r.filter(_.adjacentVertices.isEmpty)

  points.map(p => {
    p.copy(pageRank =BigDecimal(0.1*(1/5.0) + (1-0.1)*ranks.filter(p1 => p1.name == p.name).map(_.pageRank).sum).setScale(3, BigDecimal.RoundingMode.HALF_UP).toDouble
    ).pr
  })

}

case class Point(number: String, distance: Option[Int], neighbors: Vector[String]) {
  def printPoint = println(s"${number}\t${distance.getOrElse("INF")}\t${neighbors.mkString("{",",","}")}")
}

//case class Tip(name: String, minWeight: Option[Int] = None)
//case class Edge(from: String, to: String, weight: Int)

//REMOVE DUPLICATES
object ListUtil {
  def dedupe[T](elements:List[T]):List[T] = elements match {
    case Nil => elements
    case head::tail => head :: dedupe(tail filterNot (_==head))
  }
}
case class Point531(name: String, pageRank: Double,  adjacentVertices: List[String]) {
   def toString1: String = s"$name\t$pageRank\t${adjacentVertices.mkString("{",",","}")}"
  def pr = println(this.toString1)
}
