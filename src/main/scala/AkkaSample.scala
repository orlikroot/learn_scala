
import akka.actor.{Actor, ActorSystem, Props}
import akka.event.Logging

//import scala.io.StdIn


class AkkaSample extends Actor {
//  override def receive: Receive = {
//    case "printit" =>
//      val secondRef = context.actorOf(Props.empty, "second-actor")
//      println(s"Second: $secondRef")
//  }

    val log = Logging(context.system, this)
   override def receive = {
      case "tea" => log.info("Tea time!")
      case "coffee" => log.info("Coffee time!")
      case "salt" => log.info("Salt time!")
      case _ => log.info("Hmmm...")
    }
}

object AkkaSample extends App {

//  val system = ActorSystem("AkkaSystem")
//
//  val firstRef = system.actorOf(Props[AkkaSample], "first-actor")
//  println(s"First: $firstRef")
//  firstRef ! "printit"
//
//  println(">>> Press ENTER to exit <<<")
//  try StdIn.readLine()
//  finally system.terminate()

  val system = ActorSystem("drinks-system")
  val props = Props[AkkaSample]
  val drinkActor = system.actorOf(props, "drinkActor-1")
  drinkActor ! "tea"
  drinkActor ! "coffee"
  drinkActor ! "water"
  drinkActor ! "salt"
  system.terminate()

}
