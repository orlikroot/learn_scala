class PartialFunctionSample {

}


/**
  * Функция работает для каждого аргумента определенного типа. Другими словами, функция объявляется как (Int) => String, принимающая любой Int и возвращающая строку.

Частичная функция определена только для определенных значений определенного типа. Частичные функции (Int) => String не может принимать любой Int.

isDefinedAt это метод PartialFunction, который может использоваться, чтобы определить, будет ли PartialFunction принимать данный аргумент.
  */
object PartialFunctionSample extends App{

  val one: PartialFunction[Int, String] = { case 1 => "one" }
  val two: PartialFunction[Int, String] = { case 2 => "two" }
  val three: PartialFunction[Int, String] = { case 3 => "three" }
  val wildcard: PartialFunction[Int, String] = { case _ => "something else" }

  println(one.isDefinedAt(1))
//  res0: Boolean = true

  println(one.isDefinedAt(2))
//  res1: Boolean = false


  val partial = one orElse two orElse three orElse wildcard

  println(partial(5))
//  res24: String = something else

  println(partial(3))
//  res25: String = three

  println(partial(2))
//  res26: String = two

  println(partial(1))
//  res27: String = one

  println(partial(0))
//  res28: String = something else

  println(one.lift(1))
  // Some(one)

  println(one.lift(2))
  // None

  print(one(1))
  // one

//  print(one(2))
  // scala.MatchError
}
