class RegExpSample {
}

object RegExpSample extends App {

  val w = "$"
  val a = s"First $w "
  val s: String = "Lorem ipsum dolor {sit} $ amet[sit]"
  val b = a.concat(s)
  println(b.replaceAll("\\{sit}", "YAHOOOOOO"))

  val one = One(("rty  |   $ rty " +
    "gdfgdfgdfgdfgdf \n dfgdfgdfgdfgdfg    dfsgdf dfg   dfgdgdfg").toCharArray.map(char => if (char.isSpaceChar) "" else char).mkString)
  val two = One("abc {sit} def")

  println(two.value.replaceAll("\\{sit}", one.value.replaceAll("\\$", "USD")))

  println(one.value)

  println(one.value.replaceAll("USD", "\\$"))


  val contains = "\\blorem\\b".r.findFirstIn("loremipsum LOREM lorem")
  println(contains)
}

case class One(value: String)
case class Two(value: String)