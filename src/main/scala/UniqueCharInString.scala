import scala.annotation.tailrec

class UniqueCharInString {

}

object UniqueCharInString extends App {
  val txt = "SalesWings"

  def getFirstUniqueChar(s: String): Char = {
   proceed(s.toLowerCase.toList)
  }

  @tailrec
  private def proceed(x: List[Char], checked: List[Char] = Nil): Char = {
    val a = x.head
    val b = x.tail
    if (b.contains(a) || checked.contains(a)) proceed(b, a :: checked) else a
  }

  println(s"${getFirstUniqueChar(txt)}")

}
