import org.jsoup.Jsoup
import org.jsoup.parser.Parser

class JsoupExample {

}

object JsoupExample extends App {
  val html = "Lorem ipsum <a href='http://1loremipsum.io/#/donation/rty'> lorem io</a> dolor sit amet, consectetur adipiscing elit, sed " +
    "do eiusmod tempor incididunt ut <a href='http://2loremipsum.io'>lorem io</a> labore et dolore magna aliqua. Ut enim ad " +
    "minim veniam, quis nostrud exercitation <a href='http://3loremipsum.io'>lorem io</a> ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

val document = Jsoup.parse(html, "", Parser.htmlParser())
  val elements = document.select("a")
//for (element <- elements) {
//  println(element)
//}
  println(document.toString)

//  elements.forEach(e => e.attr("href",e.attr("href").concat("%%%%%%%%%%%%%%%%%%%%%%%")))

  val elementsShortLink = document.select("a[href*=donation]");
  elementsShortLink.forEach(e => {
    println(
    e.attr("href"),
    e.text())
  })

  elementsShortLink.forEach(e => {

      e.attr("href", "http://WWW.GOOGLE.COM")
      e.text("LINK IS HERE")
  })
println(document.body().html().toString)
}