class InterviewTest {

}

object InterviewTest extends App {

  type FT = (String, Int, Double)

  val list = List(10, 20, 30, 40, 50)
  val c = 25.+(44)

  println(list.foldLeft("42")(_ + " " + _))
  println(list.foldRight("42")(_ + " " + _))
  println(list.fold("42")(_ + " " + _))
  println(list.reduceRight(_ + _))
  println(list.reduceRight(_ - _))
  println(list.reduceLeft(_ - _))


  def operation[A](f: (A, A) => A)(x: A, y: A): A = {
    f(x,y)
  }

  def sum[A <: Int](x: A, y: A): A = {
    operation(summa(x,y))(x,y)
  }

  def min[A <: Int](x: A, y: A): A = {
    operation(minus(x,y))(x,y)
  }

  //TAIL RECURSION EXAMPLE
  def fact(n: Int, acc: BigInt = 1): BigInt = {
    if (n <= 1) acc
    else fact(n-1, acc * n)
  }

  println("TAIL: " + fact(5))

  //TYPE EXAMPLE
  def cc(s: FT) = {
    val (x,y,z) = s
    x + y + z
  }

  println("TYPE: " + cc("10",10,10))


  def summa[A <: Int](x: A, y: A): (A,A) => A = (x, y) => (x + y).asInstanceOf[A]
  def minus[A <: Int](x: A, y: A): (A,A) => A = (x, y) => (x - y).asInstanceOf[A]



  println(sum(28, 12))
  println(min(28, 12))

 val a: List[Ab] = List(new First, new Last)

  val y = List(1, 2, 3).map(c => c) == List(1, 2, 3)
 println(y)

  protected

  val g = Option



  def car(x: Int)(y: Int): Int = {
    x + y
  }

  "[0-9]".r.findFirstMatchIn("454656465454465") match {
    case Some(x) => println(s"SOME $x")
    case None => println("NONE")
  }

  val c1 = car(1)_
  val c2 = c1(2)
  println(c1)
  println(c2)
}



abstract class Ab
class First extends Ab
class Last extends Ab

abstract class PrintA[-A] {
  def print(value: A): Unit
}

class AbPrinter extends PrintA[Ab] {
  override def print(value: Ab): Unit = println("AB" + value)
}

class FirstPrinter extends PrintA[First] {
  override def print(value: First): Unit = println("FIRST" + value)
}

