import java.util.concurrent.TimeUnit

import MyActor.{Goodbye, Greeting}
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.util.Timeout

class AkkaSample3 {

}

object MyActor {
  case class Greeting(from: String)
  case object Goodbye
}

class MyActor extends Actor with ActorLogging {

  val system = ActorSystem("mySystem3")
  val myActor = system.actorOf(Props[MyActor], "myactor3")

  import MyActor._
  implicit val r = new Timeout(5, TimeUnit.SECONDS)
  def receive = {
    case Greeting(greeter) => {
      log.info(s"I was greeted by $greeter.")


      if (greeter == "5") myActor ! Goodbye
      log.info(s"Grated by $greeter complete")
    }
    case Goodbye => {
      log.info("Someone said goodbye to me.")
    }
  }
}

object AkkaSample3 extends App{
  val system = ActorSystem("mySystem")
  val myActor = system.actorOf(Props[MyActor], "myactor2")

  for (i <- 1 to 10) myActor ! Greeting(i.toString)
  for (i <- 1 to 10) myActor ! Goodbye
 Option
}
