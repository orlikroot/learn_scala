
class ImplicitSample {


}

object ImplicitSample extends App{

  implicit class RichInt(i: Int) {
    def twice = {
      i * i
    }
  }


  implicit def strToInt(str: String) = str.toInt + 50

  def sum(x: Int, y:Int) = x + y

  val a = "123"
  val b = 100

  println(sum(a, b).isInstanceOf[Int].toString + " " + sum(a, b) )
  //  true 273

  println(sum(100, "500").twice)
  // 422500
}