class VarianceSample {

}

object VarianceSample extends App{

  trait Animal {
    val name: String
    var position: Int

    def getName: String = this.name
  }

  class Cat(catName: String) extends Animal {

    override val name: String = catName
    override var position: Int = 0

    def move(length: Int): Int = position + length
    def moveFast(length: Int): Int = position + length * 2
  }

  class Elephant(catName: String) extends Animal {

    override val name: String = catName
    override var position: Int = 0

    def move(length: Int): Int = position + length
  }

  class Covariant[+A]

  class Contravariant[-A]

  class Invariant[A]

  var cv: Covariant[Elephant] = new Covariant[Elephant]
//  var cv_bad: Covariant[Elephant] = new Covariant[Animal]
//  var cv_bad: Covariant[Elephant] = new Covariant[Cat]

  var cv2: Covariant[Animal] = new Covariant[Animal]
  var cv2_good: Covariant[Animal] = new Covariant[Elephant]
  var cv2_good2: Covariant[Animal] = new Covariant[Cat]


  var crv: Contravariant[Elephant] = new Contravariant[Elephant]
  var crv_good: Contravariant[Elephant] = new Contravariant[Animal]
//  var crv_bad: Contravariant[Elephant] = new Contravariant[Cat]

  var crv2: Contravariant[Animal] = new Contravariant[Animal]
//  var crv2_bad: Contravariant[Animal] = new Contravariant[Elephant]
//  var crv2_bad: Contravariant[Animal] = new Contravariant[Cat]


  var iv: Invariant[Elephant] = new Invariant[Elephant]
//  var iv_bad: Invariant[Elephant] = new Invariant[Cat]
//  var iv_bad: Invariant[Elephant] = new Invariant[Animal]

  val iv2: Invariant[Animal] = new Invariant[Animal]
//  val iv2_bad: Invariant[Animal] = new Invariant[Cat]
//  val iv2_bad: Invariant[Animal] = new Invariant[Elephant]

}


