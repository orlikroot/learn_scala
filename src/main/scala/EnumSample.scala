import scala.util.Try

class EnumSample {

}

object EnumSample extends App {

  def transform[T <: Enumeration](value: String, enum: T): Either[Throwable, enum.Value] =  {
    Try(enum.withName(value)) toEither
  }

  case class ActionResult(status: Int = 200,
                           body: Any,
                           headers: Map[String, String])

  def ts[T](x: Either[Throwable, T]): ActionResult = {
    x match {
      case Right(value) => ActionResult(body = value, headers = Map.empty )
      case Left(_) => ActionResult(status = 500, body = "", headers = Map.empty)
    }
  }

  val valid = transform("false", BooleanEnum)
  val invalid = transform("false_", BooleanEnum)

  val sss = for {
    value <- transform("false", BooleanEnum)
  } yield value

  println(
    ts {
      println("Here")
      sss
    }
  )

  println(sss)
  println(ts(valid))
  println(ts(invalid))
}

object BooleanIntEnum extends Enumeration {
  type BooleanIntEnum = Value

  val FALSE = Value(0, "false")
  val TRUE = Value(1, "true")
}

object BooleanEnum extends Enumeration {
  type BooleanEnum = Value

  val FALSE = Value("false")
  val TRUE = Value("true")
}
