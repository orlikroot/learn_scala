import java.time.ZonedDateTime
import java.util.UUID




class EqualsSample {
}

object EqualsSample extends App {

//  println(1 == 1.2)

//  (1 to 30).toList.map(_ => println(UUID.randomUUID()))

//  val c = Status.New
//  val d = Status.New
//
//  println(!c.equals(Status.Old, Status.New))
//  println(c.equals(d))
//  println(c.eq(Status.New))
//  println(c.eq(d))
//  println(c.ne(Status.New))
//  println(c.ne(Status.Old, Status.Old2, Status.New))
//
//
//  println(List("1", "2", "3").mkString(","))
//  println(List("1").mkString(","))

  println("#######  " + Status.get(1))
  println("#######  " + Status.get(12))
  println("#######  " + Status.New)

}

object Status extends Enumeration {
  type Status = Value

  val New = Value(1, "new")
  val Old = Value(2, "old")
  val Old2 = Value(3, "old2")

  def get(i: Int): Option[String] = this.values.map(value => value.id -> value.toString).toMap.get(i)

}

