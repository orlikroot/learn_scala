name := "learn_scala"

version := "0.4"

scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.22"



libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion
  , "com.typesafe.akka" %% "akka-stream" % akkaVersion
  , "com.typesafe.akka" %% "akka-testkit" % akkaVersion
  , "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0"
  , "ch.qos.logback" % "logback-classic" % "1.2.3"
  , "org.jsoup" % "jsoup" % "1.11.3"
  , "org.scalatest" %% "scalatest" % "3.2.0-SNAP10"
  , "org.bytedeco" % "javacv" % "1.5"
  , "org.apache.spark" %% "spark-core" % "2.4.3"
  , "org.typelevel" %% "cats-core" % "2.0.0-RC1"
  , "com.amazonaws" % "aws-java-sdk-s3" % "1.11.642"

)